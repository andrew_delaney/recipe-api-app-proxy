# recipe-api-app-proxy

NGINX proxy app for our recipe app API

## Usage

### Environment Variables
 * `LIST_PORT` - Port to listen on (defaults: `8000`)
 * `APP_HOST` - Hostname of the app to forward requests to.  (defaults: `app`)
 * `APP_PORT` - Port of the app to forward requests to. (defaults: `9000`)
 * ``
 